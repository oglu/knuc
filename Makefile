TFLAGS=-v
TIME=@time $(TFLAGS)
NL=@echo "-"

all: knuc knuc.orig

knuc: knuc.o
	g++ -Wl,--no-as-needed -lpthread -o knuc -O4 knuc.o

knuc.o: knuc.cpp
	g++ -c knuc.cpp -march=native -fomit-frame-pointer -O4 -std=c++11 

knuc.orig: knucleotide.gpp-3.c++.o
	/usr/bin/g++ knucleotide.gpp-3.c++.o -o knuc.orig -Wl,--no-as-needed -lpthread 

knucleotide.gpp-3.c++.o: knucleotide.gpp-3.c++.cpp
	/usr/bin/g++ -c -pipe -O3 -fomit-frame-pointer -march=native  -std=c++0x knucleotide.gpp-3.c++.cpp -o knucleotide.gpp-3.c++.o 

.PHONY: time
time: all
	@echo "----- knuc time ------"
	$(TIME) ./knuc < knucleotide-input.txt > out.txt

.PHONY: time-orig
time-orig: all
	@echo "----- knuc.orig time ------"
	$(TIME) ./knuc.orig < knucleotide-input.txt > out.orig.txt

.PHONY: test
test: all
	@./knuc < knucleotide-input.txt > out.txt
	@diff -sq knucleotide-output.txt out.txt

.PHONY: clean
clean:
	rm -rf *.o knuc knuc.orig out.txt out.orig.txt
